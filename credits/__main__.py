import datetime
from decimal import Decimal

from .domain import (
    Tranche,
    Investor,
    Investment,
    Credit,
    InvestmentIntent
)
from .runner import Runner


credit = Credit(
    start_date=datetime.date(2020, 10, 1),
    end_date=datetime.date(2020, 11, 15)
)
tranche_a = Tranche(name='A', rate=3.0, capacity=Decimal('1000.0'))
tranche_b = Tranche(name='B', rate=6.0, capacity=Decimal('1000.0'))

credit.add_tranche(tranche_a)
credit.add_tranche(tranche_b)

investor1 = Investor(name='1', account=Decimal('10000.00'))
investor2 = Investor(name='2', account=Decimal('10000.00'))
investor3 = Investor(name='3', account=Decimal('10000.00'))
investor4 = Investor(name='4', account=Decimal('10000.00'))

investment_intents = [
    InvestmentIntent(
        tranche=tranche_a,
        investment=Investment(
            investor=investor1,
            amount=Decimal('1000.00'),
            date=datetime.date(2020, 10, 3)
        )
    ),
    InvestmentIntent(
        tranche=tranche_a,
        investment=Investment(
            investor=investor2,
            amount=Decimal('1.00'),
            date=datetime.date(2020, 10, 4)
        )
    ),
    InvestmentIntent(
        tranche=tranche_b,
        investment=Investment(
            investor=investor3,
            amount=Decimal('500.00'),
            date=datetime.date(2020, 10, 10)
        )
    ),
    InvestmentIntent(
        tranche=tranche_b,
        investment=Investment(
            investor=investor4,
            amount=Decimal('1100.00'),
            date=datetime.date(2020, 10, 25)
        )
    )
]

runner = Runner(
    start_date=datetime.date(2020, 10, 2),
    end_date=datetime.date(2020, 11, 2)
)
runner.add_subscriber(credit)
for intent in investment_intents:
    runner.add_subscriber(intent)

runner.run()

for intent in investment_intents:
    print(intent)

for investor in [investor1, investor3]:
    print(investor, 'has', investor.profit, 'profit')
