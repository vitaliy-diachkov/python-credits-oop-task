from calendar import monthrange
from datetime import date, timedelta
from decimal import Decimal


class DateHelper:
    @staticmethod
    def get_previous_month_first_day(date_in_month: date) -> date:
        previous_month_date = date_in_month - timedelta(days=1)
        return previous_month_date.replace(day=1)

    @staticmethod
    def get_dates_delta_in_days(end_date: date, start_date: date):
        td = end_date - start_date
        return td.days


class PercentCalculator:
    @classmethod
    def get_percents_x_of_y(cls, x, y):
        one_percent_of_y = cls.get_one_percent(y)
        return x / one_percent_of_y

    @classmethod
    def get_x_percents_of_y(cls, x, y):
        one_percent_of_y = cls.get_one_percent(y)
        return x * one_percent_of_y

    @staticmethod
    def get_one_percent(x):
        return x / 100


class MonthProfitCalculator:
    """
    Investor monthly profit calculator. It calculates the profit that investor
    has earned in previous month. The result value is calculated only for the
    actual investing period, meaning if an investor has made his investment on
    15th of January then he will get his profit only for 15-31 January.
    """

    def __init__(self, calculation_date: date):
        self._month_start = DateHelper.get_previous_month_first_day(
            calculation_date)
        self._month_end = calculation_date
        _, self._days_in_month = monthrange(
            self._month_start.year,
            self._month_start.month
        )

    def calculate(
        self,
        rate: float,
        investment: 'Investment'  # noqa: F821
    ) -> Decimal:
        investment_start_date = max(self._month_start, investment.date)
        investment_days = DateHelper.get_dates_delta_in_days(
            self._month_end,
            investment_start_date)

        investment_days_percent = PercentCalculator.get_percents_x_of_y(
            investment_days,
            self._days_in_month)
        profit_percents = PercentCalculator.get_x_percents_of_y(
            investment_days_percent,
            rate)

        profit = float(investment.amount) * profit_percents / 100
        return round(Decimal(profit), 2)
