"""
This module contains abstract classes (interfaces) that can be used
package-wide.
"""


import datetime
from abc import ABC, abstractmethod


class Publisher(ABC):
    """
    Publisher is a class that is notifying it's subscribers about any event has
    happened.
    """

    @abstractmethod
    def notify(self):
        pass


class Subscriber(ABC):
    """
    Subscriber is listening the Publisher's events. Once the event has
    happen the Publisher will call Subscriber's ``.trigger`` method.
    """

    @abstractmethod
    def trigger(self, date: datetime.date):
        pass
