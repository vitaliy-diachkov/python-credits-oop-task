import datetime

from typing import Optional

from ._base import Subscriber, Publisher


class Runner(Publisher):
    def __init__(
        self,
        start_date: datetime.date,
        end_date: Optional[datetime.date],
        delta_days: int = 1
    ):
        self._date = start_date
        self._end_date = end_date
        self._timedelta = datetime.timedelta(days=delta_days)

        self._subscribers: list[Subscriber] = []

    def notify(self):
        for subscriber in self._subscribers:
            subscriber.trigger(self._date)

    def add_subscriber(self, subscriber: Subscriber):
        self._subscribers.append(subscriber)

    def run(self):
        while self._is_running():
            self.notify()
            self._date += self._timedelta

    def _is_running(self):
        if self._end_date is not None:
            return self._date <= self._end_date
        return True
