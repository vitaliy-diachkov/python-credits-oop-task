import datetime
from decimal import Decimal

import pytest

from credits import Tranche, Investor, Investment, Credit


@pytest.fixture
def tranche() -> Tranche:
    return Tranche(name='A', rate=3.0, capacity=Decimal('1000.00'))


@pytest.fixture
def investor() -> Investor:
    return Investor(name='John Doe', account=Decimal('10000.00'))


@pytest.fixture
def investment(investor: Investor) -> Investment:
    return Investment(
        investor=investor,
        amount=Decimal('500.00'),
        date=datetime.date(2020, 10, 15)
    )


@pytest.fixture
def credit() -> Credit:
    return Credit(
        start_date=datetime.date(2015, 10, 1),
        end_date=datetime.date(2015, 11, 15)
    )