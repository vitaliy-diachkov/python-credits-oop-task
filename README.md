# OOP mastering task

The task is defined at [TASK.md](TASK.md).

## Deploy & run

Firstly, install project dependencies:

```shell
virtualenv -p python3.9 .venv
. .venv/bin/activate
pip install -r requirements.txt
```

Then run an example ...

```shell
python3 -m credits
```

... or execute tests:

```shell
pytest
```
